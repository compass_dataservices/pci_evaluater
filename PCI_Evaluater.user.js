// ==UserScript==
// @name         PCI Evaluater
// @namespace    http://compass-usa.com
// @version      0.6.5
// @updateURL    https://gist.github.com/WisdomWolf/4d845eaeacd75b26c4c76aab0226709c/raw/PCI_Evaluater.user.js
// @description  PCI Evalution Form Automater
// @author       WisdomWolf
// @match        remedyweb.compass-usa.com/arsys/forms/remedyprod/FSS%3AEstate+PCI+Evaluation/FSS+Evaluation+Form/*
// @require      https://craig.global.ssl.fastly.net/js/mousetrap/mousetrap.min.js
// @require      https://code.jquery.com/jquery-2.2.4.min.js
// @grant        GM_setClipboard
// ==/UserScript==
/* jshint -W097 */
'use strict';
var type = $('#arid_WIN_0_536870918');
var manufacturer = $('#arid_WIN_0_536870919');
var product = $('#arid_WIN_0_536870920');
var creditCards = $('#arid_WIN_0_536870931');
var merchantOfRecord = $('#arid_WIN_0_536870926');
var posVersion = $('#arid_WIN_0_536870921');
var servicePack = $('#arid_WIN_0_536870922');
var hotFix = $('#arid_WIN_0_536870923');
var terminalType = $('#arid_WIN_0_777702184');
var internetConnection = $('#arid_WIN_0_536870927');
var router = $('#arid_WIN_0_536870930');
var terminalNetwork = $('#arid_WIN_0_536870929');
var terminalHookUp = $('#arid_WIN_0_777702463');
var ccMiddleware = $('#arid_WIN_0_777702277');
var interfaceDD = $('#arid_WIN_0_536870928');
var serverAV = $('#arid_WIN_0_777702143');
var avAutoUpdate = $('#arid_WIN_0_777702085');
var serverOS = $('#arid_WIN_0_777702462');
var dialUpModem = $('#arid_WIN_0_777702085');
var modemConfig = $('#arid_WIN_0_777702191');
var vendorAccessSW = $('#arid_WIN_0_777702185');
var vendorAccessSetup = $('#arid_WIN_0_777702186');
var uniqueLogin = $('#arid_WIN_0_777702187');
var LPExceptionReporting = $('#arid_WIN_0_536870924');
var serverRestricted = $('#arid_WIN_0_536870924');
var maintenancePatch = $('#arid_WIN_0_777702144');
var masking = $('#arid_WIN_0_777702188');
var onSiteServer = $('#arid_WIN_0_777850000');
var onSiteServerLMI = $('#arid_WIN_0_777850001');
var notes = $('#arid_WIN_0_777702194');

posVersion.addClass('mousetrap');
type.bind('change', function() {
	alert('Type change detected');
	fillDefaults();
});

function updateField(element, value) {
	element.val(value);
	triggerChange(element);
}

function triggerChange(element) {
	if (element.length != undefined) {
		element = element[0];
	}
	if ("createEvent" in document) {
		var evt = document.createEvent("HTMLEvents");
		evt.initEvent("change", false, true);
		element.dispatchEvent(evt);
	} else {
		element.fireEvent("onchange");
	}
}

function markFieldForReview(field) {
	field.css('background-color', 'yellow');
	field.bind('focusout', function() {
		field.css('background-color', '');
	});
}

function fillDefaults() {
	console.log('Filling Defaults');
	updateField(creditCards, 'Yes');
	updateField(merchantOfRecord, 'Compass');
	markFieldForReview(internetConnection);
	markFieldForReview(router);
	markFieldForReview(terminalNetwork);
	markFieldForReview(vendorAccessSetup);
	updateField(terminalHookUp, 'Wired');
	updateField(interfaceDD, 'N/A');
	updateField(serverAV, 'N/A');
	updateField(avAutoUpdate, 'N/A');
	updateField(serverOS, 'N/A');
	updateField(dialUpModem, 'N/A');
	updateField(modemConfig, 'N/A');
	updateField(vendorAccessSW, 'FootPrints');
	updateField(uniqueLogin, 'N/A');
	updateField(LPExceptionReporting, 'Yes');
	updateField(maintenancePatch, 'Vendor');
	updateField(onSiteServer, 'N/A');
	updateField(onSiteServerLMI, 'N/A');
	
	if (product.val() == 'Simphony') {
		updateField(posVersion, '2.7');
		updateField(servicePack, '4');
		updateField(hotFix, '4');
		updateField(terminalType, 'WS6');
	} else if (product.val() == 'Infogenesis') {
		updateField(posVersion, '4.4.7');
		updateField(servicePack,'N/A');
		updateField(hotFix, 'N/A');
		updateField(terminalType, 'J2');
	}
}

Mousetrap.bind('alt+u', function() {
	//alert('Updating Dashboard via UserScript');
	fillDefaults();
});